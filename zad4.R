library("C50")

dane <- read.csv('macbook.csv')
dane$Ocena <- factor(dane$Ocena)
head(dane)
treeModel <- C5.0(x=dane[,-6], y=dane$Ocena)
treeModel
summary(treeModel)
plot(treeModel)